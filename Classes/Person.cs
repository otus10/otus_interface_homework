﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_interface_homework
{
   public class Person : IComparable
   {
      public string FirstName { get; set; }
      public string LastName { get; set; }
      public DateTime BirthDate { get; set; }

      public int CompareTo(object obj)
      {
         if (obj is Person person)
         {
            int result = this.LastName.CompareTo(person.LastName);
            if (result == 0)
            {
               result = this.FirstName.CompareTo(person.FirstName);
            }
            return result;
         }
         throw new ArgumentException("obj is not an Person object.");
      }
   }
}

