﻿using otus_interface_homework.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace otus_interface_homework
{
   public class OtusJsonSerializer<T> : ISerializer<T>
   {
      public T Deserialize<T>(Stream stream)
      {
         JsonSerializer serializer = new JsonSerializer();
         using (StreamReader streamReader = new StreamReader(stream))
         {
            using (JsonTextReader jsonTextReader = new JsonTextReader(streamReader))
            {
               return (T)serializer.Deserialize(streamReader, typeof(T));
            }
         }
      }
      public string Serialize<T>(T item)
      {
         return JsonConvert.SerializeObject(item);
      }
   }
}
