﻿using otus_interface_homework.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_interface_homework
{
   public class AccountService : IAccountService
   {
      private const int _minAge = 18;
      private readonly IRepository<Account> _repository;

      public AccountService(IRepository<Account> repository)
      {
         _repository = repository;
      }
      public void AddAccount(Account account)
      {
         CheckAccount(account);
         _repository.Add(account);
      }

      private void CheckAccount(Account account)
      {
         if (account == null)
            throw new Exception("Account is null");
         CheckLastName(account);
         CheckFirstName(account);
         CheckAge(account);
      }
      private void CheckLastName(Account account)
      {
         if (string.IsNullOrEmpty(account.LastName))
            throw new Exception("LastName is null or empty.");
      }
      private void CheckFirstName(Account account)
      {
         if (string.IsNullOrEmpty(account.FirstName))
            throw new Exception("FirstName is null or empty.");
      }
      private void CheckAge(Account account)
      {
         if (CalculateAge(account.BirthDate) < _minAge)
            throw new Exception("Age is below 18.");
      }

      private int CalculateAge(DateTime dateOfBirth)
      {
         int age = DateTime.Now.Year - dateOfBirth.Year;
         if (DateTime.Now.DayOfYear < dateOfBirth.DayOfYear)
         {
            age = age - 1;
         }
         return age;
      }
   }
}
