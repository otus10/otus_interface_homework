﻿using otus_interface_homework.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_interface_homework
{
   public class FileStorage<T> : IRepository<T>
   {
      public string FilePath { get; set; }
      public string FileName { get; set; }
      public string FileFullPath { get => Path.Combine(FilePath, FileName); }

      public FileStorage(string filePath, string fileName)
      {
         FilePath = filePath;
         FileName = fileName;
      }

      public FileStorage(string fileFullPath) : this(Path.GetDirectoryName(fileFullPath), Path.GetFileName(fileFullPath))
      {
      }

      public Stream ReadFile()
      {
         return File.OpenRead(FileFullPath);
      }

      public void WriteFile<T>(string text)
      {
         using (FileStream fstream = new FileStream(FileFullPath, FileMode.OpenOrCreate))
         {
            byte[] array = System.Text.Encoding.Default.GetBytes(text);
            fstream.Write(array, 0, array.Length);
         }
      }
      public IEnumerable<T> GetAll()
      {
         OtusJsonSerializer<T> otusJsonSerializer = new OtusJsonSerializer<T>();
         using (Stream stream = this.ReadFile())
         {
            OtusStreamReader<T> otusStreamReader = new OtusStreamReader<T>(stream, otusJsonSerializer);
            foreach (T t in otusStreamReader)
            {
               yield return t;
            }
         }
      }
      public T GetOne(Func<T, bool> predicate)
      {
         return this.GetAll().Where(w => predicate(w)).FirstOrDefault();
      }

      public void Add(T item)
      {
         List<T> items = this.GetAll().ToList<T>();
         items.Add(item);
         OtusJsonSerializer<T> otusJsonSerializer = new OtusJsonSerializer<T>();
         this.WriteFile<T>(otusJsonSerializer.Serialize<List<T>>(items));
      }

      public void Clear()
      {
         using (FileStream fstream = new FileStream(FileFullPath, FileMode.Truncate)) { }
      }

   }
}
