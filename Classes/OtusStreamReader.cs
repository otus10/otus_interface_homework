﻿using otus_interface_homework.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_interface_homework
{
   public class OtusStreamReader<T> : IEnumerable<T>, IDisposable
   {
      private Stream _stream;
      ISerializer<T> _serializer;
      public OtusStreamReader(Stream stream, ISerializer<T> serializer)
      {
         _stream = stream;
         _serializer = serializer;
      }
      public void Dispose()
      {
         _stream.Dispose();
      }

      public IEnumerator<T> GetEnumerator()
      {
         foreach (T result in _serializer.Deserialize<T[]>(_stream))
         {
            yield return result;
         }
      }
      private IEnumerator GetEnumeratorPrivate()
      {
         return this.GetEnumerator();
      }
      IEnumerator IEnumerable.GetEnumerator()
      {
         return GetEnumeratorPrivate();
      }
   }
}
