﻿using System.IO;

namespace otus_interface_homework.Interfaces
{
   public interface ISerializer<T>
   {
      string Serialize<T>(T item);
      T Deserialize<T>(Stream stream);
   }
}