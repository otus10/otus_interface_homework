﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_interface_homework.Interfaces
{
   public interface IRepository<T>
   {
      IEnumerable<T> GetAll();
      T GetOne(Func<T, bool> predicate);
      void Add(T item);
   }
}
