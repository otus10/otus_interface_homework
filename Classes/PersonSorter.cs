﻿using otus_interface_homework.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_interface_homework
{
   public class PersonSorter : ISorter<Person>, IAlgorithm<Person>
   {
      public IEnumerable<Person> Sort<Person>(IEnumerable<Person> notSortedItems)
      {
         return notSortedItems.OrderBy(o => o).AsEnumerable<Person>();
      }
      public IEnumerable<Person> SortDescending<Person>(IEnumerable<Person> notSortedItems)
      {
         return notSortedItems.OrderByDescending(o => o).AsEnumerable<Person>();
      }
   }
}
