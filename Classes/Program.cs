﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_interface_homework
{
   class Program
   {
      static void Main(string[] args)
      {
         string fileFullPath = @"C:\\Users\\xama8\\OneDrive\\Рабочий стол\\Помойка\\test1.json";
         Person[] persons = { new Person { FirstName = "Aleksandr", LastName = "Pupkin" },
                              new Person { FirstName = "Ivan", LastName = "Ivanov" },
                              new Person { FirstName = "Petr", LastName = "Sidorov" }
                            };

         FileStorage<Person[]> fileStorageWrite = new FileStorage<Person[]>(fileFullPath);
         OtusJsonSerializer<Person[]> otusJsonSerializer = new OtusJsonSerializer<Person[]>();
         fileStorageWrite.Clear();
         fileStorageWrite.WriteFile<Person[]>(otusJsonSerializer.Serialize<Person[]>(persons));

         Console.WriteLine($"------raw data--------------------\n");

         foreach (Person person in persons)
         {
            Console.WriteLine($"Lastname: {person.LastName}, FirstName: {person.FirstName}");
         }

         Console.WriteLine($"\n------sorted data-----------------\n");

         PersonSorter personSorter = new PersonSorter();
         IEnumerable<Person> personsSorted = personSorter.Sort<Person>(persons);
         foreach (Person person in personsSorted)
         {
            Console.WriteLine($"Lastname: {person.LastName}, FirstName: {person.FirstName}");
         }

         Console.WriteLine($"\n------sorted descending data------\n");

         IEnumerable<Person> personsSortedDesc = personSorter.SortDescending<Person>(persons);
         foreach (Person person in personsSortedDesc)
         {
            Console.WriteLine($"Lastname: {person.LastName}, FirstName: {person.FirstName}");
         }

         Console.WriteLine($"\n------read data--------------------\n");

         FileStorage<Person> fileStorage = new FileStorage<Person>(fileFullPath);
         foreach (Person person in fileStorage.GetAll())
         {
            Console.WriteLine($"Lastname: {person.LastName}, FirstName: {person.FirstName}");
         }

         Console.WriteLine($"\n------add item--------------------\n");

         Person personAdd = new Person { FirstName = "Nikolay", LastName = "I" };
         fileStorage.Add(personAdd);
         foreach (Person person in fileStorage.GetAll())
         {
            Console.WriteLine($"Lastname: {person.LastName}, FirstName: {person.FirstName}");
         }

         Console.WriteLine($"\n------get one--------------------\n");

         Func<Person, bool> getIvanov = (Person person) => { return person.LastName == "Ivanov" ? true : false; };
         Person personIvanov = fileStorage.GetOne(getIvanov);
         if (personIvanov == null)
         {
            Console.WriteLine($"Person is not found");
         }
         else
         {
            Console.WriteLine($"Lastname: {personIvanov.LastName}, FirstName: {personIvanov.FirstName}");
         }

         Console.ReadKey();
      }
   }
}

