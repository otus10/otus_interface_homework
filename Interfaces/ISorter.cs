﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace otus_interface_homework.Interfaces
{
   public interface ISorter<T>
   {
      IEnumerable<T> Sort<T>(IEnumerable<T> notSortedItems);
   }
}
